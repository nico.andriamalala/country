import { createAppContainer, createStackNavigator } from "react-navigation";
import CountryList from '../component/CountryList';
import CountryDetail from '../component/CountryDetail';

export default createAppContainer(createStackNavigator({
  CountryList: {
    screen: CountryList,
    navigationOptions: {
      title: 'Liste Pays',
    }
  },
  CountryDetail:{
    screen : CountryDetail,
    navigationOptions:{
      title:'Detail Pays'
    }
  }
}, {
  defaultNavigationOptions: {
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#283e4a',
    },
  }
}
));