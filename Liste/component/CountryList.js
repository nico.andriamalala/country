import React, { Component } from 'react';
import { View, FlatList,Text,StyleSheet } from 'react-native';
import {countries} from '../api/data';
import CountryItem from './CountryItem';
import Loader from './Loader'
import {getCountries} from '../api/restcountries'

export default class CountryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        countries:[],
        isLoading : true
    };
  }

  componentDidMount(){
    getCountries()
      .then((result)=>{
        this.setState({countries : result})
      })
      .catch((error)=>{console.log(error)})
      .finally(()=>{this.setState({isLoading:false})})
  }

  render() {
    return (
        <FlatList
          style={styles.list}
          data={this.state.countries}
          keyExtractor={(item) => item.name.toString()}
          renderItem={({item}) => (
            <CountryItem
                country={item}
            />
          )}
        />
    );
  }
}

const styles = StyleSheet.create({
    list: {
      flex: 1
    }
})
