import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

export default class ContryDetail extends React.Component {
constructor(props){
  super(props);
  
}

  render() {
    return (
      <View style={styles.container}>
        <Text>Detail {this.props.country.name} : </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
