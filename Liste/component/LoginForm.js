import React from 'react';
import {
    StyleSheet,
    View,
    TextInput,
    Button,
    Text,
} from 'react-native';


export default class LoginForm extends React.Component {
    render() {
        const {connect,setUsername,setPassword} = this.props
        return (
        <View style={styles.container} behavior="padding">
        <TextInput style={styles.input} 
            placeholder="Nom d'Utilisateur"
            returnKeyType="next"
            returnKeyLabel="Suivant"
            autoCapitalize="none"
            autoCapitalize="none"
            onSubmitEditing={()=>this.passwordInput.focus()}
            onChangeText={(text)=>setUsername(text)}
            />
        <TextInput 
            style={styles.input} 
            placeholder="Mot de passe"
            secureTextEntry
            returnKeyType="go"
            returnKeyLabel="Connect.."
            onChangeText={(text)=>setPassword(text)}
            
            ref={(input) => this.passwordInput = input}
        />
        <View style={styles.buttonContainer}>
        <Button
            style={styles.button}
            onPress={()=>connect()}
            title="Se Connecter"
            color="#00aa9c"
            accessibilityLabel="Connexion job-database.mg"
            />
            <View style={styles.or}>
                <Text>Ou</Text>
            </View>
            <Button
                style={styles.button}
                onPress={onPressLearnMore}
                title="S'inscrire"
                color="#e74c3c"
                accessibilityLabel="Inscription job-database.mg"
            />
            </View>
        </View>
        );
    }
}

var onPressLearnMore = function(){
    return;
}
const styles = StyleSheet.create({
    or:{
        padding:20,
        alignItems:'center'
    },
    buttonContainer:{
        paddingTop:20,
        paddingBottom:20,
        width:300,
    },  
    inscriptionButton:{
        fontWeight:'700',
    },
    button:{
        flex:1,
        width:250
    },
    input :{
        width:300,
        height:60,
        marginBottom:20,
        backgroundColor: '#FFF',
        paddingHorizontal : 10,
    },  
  container: {
    padding:5,
    alignItems:"center"
  }
});
