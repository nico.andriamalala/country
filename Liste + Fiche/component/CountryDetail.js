import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';


export default class ContryDetail extends React.Component {
constructor(props){
  super(props);
  this.country = this.props.navigation.state.params.country;
}

  render() {
    return (
      <View style={styles.container}>
        <Image
            style={styles.image}
            source={{uri: 'https://www.countryflags.io/'+this.country.alpha2Code+'/flat/64.png'}}
          />
        <Text style={styles.default_text}> Nom : {this.country.name}</Text>
        <Text style={styles.default_text}> Capitale : {this.country.capital}</Text>
        <Text style={styles.default_text}> Population : {this.country.population}</Text>
        <Text style={styles.default_text}> Superficie : {this.country.area}</Text>
        <Text style={styles.default_text}> Monaie : {this.country.currencies[0].name} ({this.country.currencies[0].code})</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  scrollview_container: {
    flex: 1
  },
  image: {
    height: 169,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: '#000000',
    textAlign: 'center'
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
  }
})
