import React from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text,
  KeyboardAvoidingView,
  Alert
} from 'react-native';
import LoginForm from './LoginForm';
import {connectUser} from '../api/JDAPI';
import { connect } from 'react-redux'
const mapStateToProps = (state) => {
  return state
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}
class LoginScreen extends React.Component {
  constructor(props){
    super(props)
    this.username=""
    this.password=""

    this._connect = this._connect.bind(this)
    this._setUsername = this._setUsername.bind(this)
    this._setPassword = this._setPassword.bind(this)
  }
  static navigationOptions = {
    header: null,
  };
  _setUsername = (text) =>{
    this.username = text;
  }
  _setPassword = (text) =>{
    this.password = text;
  }
  _connect = () => {
    connectUser(this.username,this.password)
      .then((response)=>{
        if(response.success){
          let token = response.data.token;
          let userInfo = response.data.userInfo;

          const action = { type: "TOGGLE_TOKEN", value: token }
          this.props.dispatch(action)

          this.props.navigation.navigate('App',{token:token,userInfo:userInfo});
        }else{
          Alert.alert(
            'Erreur !',
            response.message,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
      })
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View style={styles.logoContainer}>
        <Text></Text>
        <Text style={styles.title}>Veuillez vous Identifiez</Text>
        </View>
        <View style={styles.formController}>
            <LoginForm 
              connect={this._connect}
              setUsername={this._setUsername}
              setPassword={this._setPassword}
            />
        </View>
      </KeyboardAvoidingView>
    );
  }

}

const styles = StyleSheet.create({
    formController:{

    },
    logoContainer:{
        flexGrow:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo :{
        
    },
    title :{
        marginTop:10,
        textAlign: 'center'
    },
  container: {
    marginTop :20,
    flex: 1,
    backgroundColor: '#fff',
  }
});
  
export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen)