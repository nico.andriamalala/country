import React from 'react';
import { StyleSheet, Text, View,Image,TouchableOpacity } from 'react-native';

export default class CountryItem extends React.Component {
  constructor(props){
    super(props);
    this.country = this.props.country
    this.detailCountry = this.props.detailCountry;
  }

  

  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={()=>{this.detailCountry(this.country)}}>
        <Image
          style={styles.image}
          source={{uri:'https://www.countryflags.io/'+this.country.alpha2Code+'/flat/64.png'}}
        />
        <Text style={styles.countryName}> {this.country.name}</Text>
      </TouchableOpacity>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginTop:5,
    marginBottom:5,
  },
  image:{
    width:40,
    height:40
  },
  countryName:{
    flex : 1,
    fontWeight: 'bold',
    marginTop: 10,
  }
});
