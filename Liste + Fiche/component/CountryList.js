import React, { Component } from 'react';
import { View, FlatList,Text,StyleSheet } from 'react-native';
import {countries} from '../api/data';
import CountryItem from './CountryItem';
import Loader from './Loader'
import {getCountries} from '../api/restcountries'

export default class CountryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        countries:[],
        isLoading : true
    };
    this._detailCountry = this._detailCountry.bind(this);
  }

  componentDidMount(){
    getCountries()
      .then((result)=>{
        this.setState({countries : result})
      })
      .catch((error)=>{console.log(error)})
      .finally(()=>{this.setState({isLoading:false})})
  }

  _detailCountry(country){
    this.props.navigation.navigate('CountryDetail',{'country':country})
  }

  render() {
    return (
        <FlatList
          style={styles.list}
          data={this.state.countries}
          keyExtractor={(item) => item.name.toString()}
          renderItem={({item}) => (
            <CountryItem
                country={item}
                detailCountry={this._detailCountry}
            />
          )}
        />
    );
  }
}

const styles = StyleSheet.create({
    list: {
      flex: 1
    }
})
