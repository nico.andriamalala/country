export function getCountries(){
    const url = "https://restcountries.eu/rest/v2/all";
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.log(error))
}